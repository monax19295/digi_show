import requests

     
                
def call(fr, lim, ye):
    cookies = {
        '_ym_uid': '15623996221045461206',
        '_ym_d': '1562399622',
        '_ym_isad': '2',
        '_ym_wasSynced': '%7B%22time%22%3A1562399624288%2C%22params%22%3A%7B%22eu%22%3A0%7D%2C%22bkParams%22%3A%7B%7D%7D',
        '_ym_hostIndex': '0-2%2C1-0',
        '_ym_visorc_50955743': 'w',
        '7a8abfa77a4da81bf5f3b2f26dc44364': 'f24b06f499517f0a23558bd69c6afac6',
    }

    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0',
        'Accept': '*/*',
        'Accept-Language': 'en-US,en;q=0.5',
        'Referer': 'http://dom.tyumen-city.ru/category/?fks=3971',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'DNT': '1',
        'Connection': 'keep-alive',
    }

    params = (
        ('collection', 'sms'),
        ('action', 'registry'),
    )

    data = {
    'year': ye,
    'kinds': '',
    'status': '-1',
    'sort': 'sms',
    'uprava': '-1',
    'user': 'false',
    'start': fr,
    'limit': lim
    }

    response = requests.post('http://dom.tyumen-city.ru/api/sms/json', headers=headers, params=params, cookies=cookies, data=data)
    r = response.json()
    return r




import pandas as pd
import psycopg2
import uuid
  
def insert(word, cat):
    connection = ""
    try:
        connection = psycopg2.connect(user="postgres",
                                    password="secret",
                                    host="192.168.43.7",
                                    port="5432",
                                    database="gkh")
        cursor = connection.cursor()

        # insert text
        postgres_insert_query = """ INSERT INTO "gkh".category_word_text (ID, WORD, NAME) VALUES (%s,%s,%s)"""
        record_to_insert = (str(uuid.uuid4()), word, cat)
        cursor.execute(postgres_insert_query, record_to_insert)

        #insert words
        for w in word.split(' '):
            postgres_insert_query = """ INSERT INTO "gkh".category_word (ID, WORD, NAME) VALUES (%s,%s,%s)"""
            record_to_insert = (str(uuid.uuid4()), w, cat)
            cursor.execute(postgres_insert_query, record_to_insert)

        connection.commit()
        count = cursor.rowcount
        #print (count, "Record inserted successfully")
  
    except (Exception, psycopg2.Error) as error :
        if(connection):
            print("Failed to insert record into mobile table", error)
    
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

years = ['2014']


for year in years:
    dat = [('text1', 'cat1')]
    data = pd.DataFrame(dat, columns = ['word', 'cat'])
    do_call = True
    count_step = 40
    start_count = 1
    while do_call:
        print("DOCALL " + str(start_count) + ' y:' + str(year))
        r = call(start_count, count_step, year)
        if 'lenta' not in r or 'info' not in r['lenta'][0]['sms'] or not do_call:
            do_call = False
        else:
            r = r['lenta']
            for i in range(0, len(r)):
                t = r[i]['sms']['info']
                c = r[i]['kinds']['name']
                print('orig ' + t)
                t = prepare_array(t)
                #print('prep ' + t)
                #t = t[0].split(' ')
                #print("text")
                #print(t)
                #print("cat")
                #print(c)
                #print('\n\n')
                #print(t)
                #for word in t.split(' '):
                insert(t, c)
                    #data = data.append({'word':word, 'cat': c}, ignore_index=True)
                # remove it
                #do_call = False
            start_count += count_step
            #exit()
    #data.to_csv(r'file_'+year+'.csv', sep=';')
    


